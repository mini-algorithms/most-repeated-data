package main

import "fmt"

func main() {
	//** veriables **//
	wordArray := []string{"apple", "pie", "apple", "red", "red", "red"}
	//DESC - We call the function that finds the most frequently repeating word
	mostFrequent := mostAgain(wordArray)

	//DESC - We print the most frequently repeating word
	fmt.Println(mostFrequent)
}

//NOTE - The time complexity of this code is O(n).

func mostAgain(wordArray []string) string {
	//DESC - We create a map to store the words and the number of times they are repeated
	var most = make(map[string]int)

	//DESC - We count the number of times each word is repeated
	for i := 0; i < len(wordArray); i++ {
		//DESC - If the word has previously been found in the dictionary, increase the value of the word by 1. If not, add the word and set its value to 1.
		if v, ok := most[wordArray[i]]; ok {
			most[wordArray[i]] = v + 1
		} else {
			most[wordArray[i]] = 1
		}
	}

	// DESC - We find the index of the most repetitive word.
	var mostFrequent string
	var maxCount int
	for word, count := range most {
		if count > maxCount {
			mostFrequent = word
			maxCount = count
		}
	}

	return mostFrequent
}
